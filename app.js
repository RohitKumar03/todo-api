let form = document.getElementById("add-item");
let inputItem = document.getElementById("output-item");
let filterItem = document.getElementById("search-input");

const url = 'https://jsonplaceholder.typicode.com/todos'
const getData = async() => {
    try {
        const response = await fetch(url, { mathod: "GET" });
        const data = await response.json();
        console.log(data);
        let output = ''

        for (let i in data) {
            output += `
            <tr>
                <td>${data[i].id}</td>
                <td>${data[i].title}</td>
            </tr>`

        }
        document.getElementById("table").innerHTML = output

    } catch (error) {
        console.log(error);
    }
}

document.getElementById("btn-1").addEventListener("click", getData)